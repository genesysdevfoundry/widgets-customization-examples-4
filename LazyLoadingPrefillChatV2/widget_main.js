/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

/***********************************************************************
 * Widgets Customization Examples IV - Chat and Bot Service sample
 * 
 * Base code and page,
 * With Sidebar, Channel Selector, WebChat and TestExtension widgets configured
 * Providing buttons to open WebChat, WebChat with BotService parameters, ChannelSelector and TestExtension
 * Providing auto-fill of Chat Registration Form (using before() method)
 * Providing support for Chat v2 (polling, cometd long-polling, websocket)
 * 
 ************************************************************************/

var localWidgetPlugin;

// ##### Document Define Local Customization

// Force preload of WebChat Widget
// VALUE MUST BE CHANGED IN THIS FILE TO TAKE EFFECT
var iPreloadChatWidget = false;

// Once Chat Service is configured to use CometD, it can't be reconfigured to disable CometD (except Comet URL and Channel)
var iChatV2ServiceConfig = {
    // 0 - standard/polling, 1 - cometd with long-polling, 2 - cometd with websocket
    mode: 0,
    baseURL: "https://gmshost",
    services: {
        'Service1': 'chat-service1',
        'Service2': 'chatbot-service2'
    }
    // 'YOUR GMS CHAT SERVICE URL - https://gmshost/genesys/2/chat/chat-service1'
    // 'YOUR GMS CHAT SERVICE COMET URL -https://gmshost/genesys/cometd'
    // 'YOUR GMS CHAT SERVICE COMET CHANNEL - /service/chatV2/chat-service1'
};

var iStatsServiceDataURL = 'YOUR GMS STATS SERVICE URL';

var iFirstName = "Bill";
var iLastName = "Moris";
var iEmail = "bmoris@demo.com";
var iChatSubject = "This is a custom chat subject";
var iAutoSubmit = false;

var iChatBotID = "EchoBot";
var iChatBotName = "";
var iNickname = "Echo Bot";
var iVisibility = "ALL";
var iChatBotHoldup = true;
var iStopBotOnAgentArrival = true;
var iStopBotOnCustomerLeft = true;
// Custom Parameter to be sent in ESP StartBot request - RichMedia/StandardResponse specific
var iMediaOrigin = "facebook-messenger";

var iChatBotRegFormDef = {
    wrapper: "<table></table>",
    inputs: [
        {
            id: "cx_webchat_form_firstname",
            name: "firstname",
            maxlength: "100",
            placeholder: "@i18n:webchat.ChatFormPlaceholderFirstName",
            label: "@i18n:webchat.ChatFormFirstName"
        },
        {
            id: "cx_webchat_form_lastname",
            name: "lastname",
            maxlength: "100",
            placeholder: "@i18n:webchat.ChatFormPlaceholderLastName",
            label: "@i18n:webchat.ChatFormLastName"
        },
        {
            id: "cx_webchat_form_email",
            name: "email",
            maxlength: "100",
            placeholder: "Made mandatory via validate",
            label: "@i18n:webchat.ChatFormEmail",
            type: "text",
            validateWhileTyping: false, // default is false
            validate: function (event, form, input, label, $, CXBus, Common) {
                if (input) {
                    if (input.val())
                        return true;
                    else
                        return false;
                }
                return false;
            }
        },
        {
            id: "cx_webchat_form_subject",
            name: "subject",
            placeholder: "@i18n:webchat.ChatFormPlaceholderSubject",
            label: "Subject",
            type: "text"
        },
        {
            id: "cx_webchat_form_chatbotid",
            name: "chatBotID",
            label: "ChatBotID",
            type: "text"
        },
        {
            id: "cx_webchat_form_chatbotname",
            name: "chatBotName",
            label: "ChatBotName",
            type: "text"
        },
        {
            id: "cx_webchat_form_chatbotnickname",
            name: "chatBotNickname",
            label: "Nickname",
            type: "text"
        },
        {
            id: "cx_webchat_form_visibility",
            name: "chatBotVisibility",
            label: "Visibility",
            type: "select",
            options: [
                {
                    text: "ALL",
                    value: "ALL"
                },
                {
                    text: "INT",
                    value: "INT"
                },
                {
                    text: "VIP",
                    value: "VIP"
                }
            ],
            wrapper: "<tr><th>{label}</th><td>{input}</td></tr>"
        },
        {
            id: "cx_webchat_form_holdup",
            name: "chatBotHoldup",
            label: "ChatBotHoldup",
            type: "select",
            options: [
                {
                    text: "false",
                    value: "false"
                },
                {
                    text: "true",
                    value: "true"
                }
            ],
            wrapper: "<tr><th>{label}</th><td>{input}</td></tr>"
        },
        {
            id: "cx_webchat_form_stopagent",
            name: "stopBotOnAgentArrival",
            label: "StopBotOnAgentArrival",
            type: "select",
            options: [
                {
                    text: "false",
                    value: "false"
                },
                {
                    text: "true",
                    value: "true"
                }
            ],
            wrapper: "<tr><th>{label}</th><td>{input}</td></tr>"
        },
        {
            id: "cx_webchat_form_stopcustomer",
            name: "stopBotOnCustomerLeft",
            label: "StopBotOnCustomerLeft",
            type: "select",
            options: [
                {
                    text: "false",
                    value: "false"
                },
                {
                    text: "true",
                    value: "true"
                }
            ],
            wrapper: "<tr><th>{label}</th><td>{input}</td></tr>"
        },
        {
            id: "cx_webchat_form_chatbotcategory",
            name: "ChatBotCategory",
            label: "ChatBotCategory",
            type: "hidden",
            value: "Category"
        },
        {
            id: "cx_webchat_form_chatbotfunction",
            name: "ChatBotFunction",
            label: "ChatBotFunction",
            type: "hidden",
            value: "Function"
        },
        {
            id: "cx_webchat_form_mediaorigin",
            name: "chatBotMediaOrigin",
            label: "MediaOrigin",
            type: "text"
        }
    ]
};

var UI_Init = function () {
    $('#ChatSettings #formChatWidgetPreload').prop('checked', iPreloadChatWidget);
    $('#ChatSettings #formChatBaseURL').val(iChatV2ServiceConfig.baseURL);
    $('#ChatSettings #formChaV2Mode').val(iChatV2ServiceConfig.mode);
    $('#ChatSettings #formStatsServiceDataURL').val(iStatsServiceDataURL);

    $.each(iChatV2ServiceConfig.services, function (val, text) {
        $('#selectChatService').append($('<option></option>').val(text).html(val));
    });

    $('#PreChatSurvey #formFirstName').val(iFirstName);
    $('#PreChatSurvey #formLastName').val(iLastName);
    $('#PreChatSurvey #formEmail').val(iEmail);
    $('#PreChatSurvey #formSubject').val(iChatSubject);
    $('#PreChatSurvey #formAutoSubmit').prop('checked', iAutoSubmit);

    $('#PreBotSurvey #formChatBotID').val(iChatBotID);
    $('#PreBotSurvey #formChatBotName').val(iChatBotName);
    $('#PreBotSurvey #formChatBotNickname').val(iNickname);
    $('#PreBotSurvey #formChatBotVisibility').val(iVisibility);
    $('#PreBotSurvey #formChatBotHoldup').prop('checked', iChatBotHoldup);
    $('#PreBotSurvey #formStopBotOnAgentArrival').prop('checked', iStopBotOnAgentArrival);
    $('#PreBotSurvey #formStopBotOnCustomerLeft').prop('checked', iStopBotOnCustomerLeft);
    $('#PreBotSurvey #formMediaOrigin').val(iMediaOrigin);
}

var UI_SetWidgetsConfig = function () {
    window._genesys.widgets.stats.ewt.dataURL = $('#ChatSettings #formStatsServiceDataURL').val();

    let vBaseURL = $('#ChatSettings #formChatBaseURL').val();
    let vSelectedChatService = $('#selectChatService').find(":selected").val();

    // 'YOUR GMS CHAT SERVICE URL - https://gmshost/genesys/2/chat/chat-service1'
    let vDataURL = vBaseURL + '/genesys/2/chat/' + vSelectedChatService;
    // 'YOUR GMS CHAT SERVICE COMET URL -https://gmshost/genesys/cometd'
    let vCometURL = vBaseURL + '/genesys/cometd';
    // 'YOUR GMS CHAT SERVICE COMET CHANNEL - /service/chatV2/chatbot-service1'
    let vCometChannel = '/service/chatV2/' + vSelectedChatService;

    window._genesys.widgets.webchat.dataURL = vDataURL;
    window._genesys.widgets.webchat.cometD.cometURL = vCometURL;
    window._genesys.widgets.webchat.cometD.channel = vCometChannel;

    let vChatV2Mode = $('#ChatSettings #formChaV2Mode').val();
    if (vChatV2Mode == 0) {
        window._genesys.widgets.webchat.uploadsEnabled = true;
        window._genesys.widgets.webchat.cometD.enabled = false;
        window._genesys.widgets.webchat.cometD.websocketEnabled = false;
    } else if (vChatV2Mode == 1) {
        window._genesys.widgets.webchat.uploadsEnabled = false;
        window._genesys.widgets.webchat.cometD.enabled = true;
        window._genesys.widgets.webchat.cometD.websocketEnabled = false;
    } else if (vChatV2Mode == 2) {
        window._genesys.widgets.webchat.uploadsEnabled = false;
        window._genesys.widgets.webchat.cometD.enabled = true;
        window._genesys.widgets.webchat.cometD.websocketEnabled = true;
    }
}

var UI_ChatForm_Standard = function (iData) {

    // Assume iData is not null

    // Override iData.form
    iData.form = {
        autoSubmit: $('#PreChatSurvey #formAutoSubmit').is(':checked'),
        firstname: $('#PreChatSurvey #formFirstName').val(),
        lastname: $('#PreChatSurvey #formLastName').val(),
        email: $('#PreChatSurvey #formEmail').val(),
        subject: $('#PreChatSurvey #formSubject').val()
    };

    // iData.userData kept intact

}

var UI_ChatForm_BotService = function (iData) {

    // Assume iData is not null

    // Override iData.formJSON
    iData.formJSON = iChatBotRegFormDef;

    for (let i = 0; i < iData.formJSON.inputs.length; i++) {
        switch (iData.formJSON.inputs[i].name) {
            case "firstname":
                iData.formJSON.inputs[i].value = $('#PreChatSurvey #formFirstName').val();
                break;
            case "lastname":
                iData.formJSON.inputs[i].value = $('#PreChatSurvey #formLastName').val();
                break;
            case "email":
                iData.formJSON.inputs[i].value = $('#PreChatSurvey #formEmail').val();
                break;
            case "subject":
                iData.formJSON.inputs[i].value = $('#PreChatSurvey #formSubject').val();
                break;
            case "chatBotID":
                iData.formJSON.inputs[i].value = $('#PreBotSurvey #formChatBotID').val();
                break;
            case "chatBotName":
                iData.formJSON.inputs[i].value = $('#PreBotSurvey #formChatBotName').val();
                break;
            case "chatBotNickname":
                iData.formJSON.inputs[i].value = $('#PreBotSurvey #formChatBotNickname').val();
                break;
            case "chatBotVisibility":
                iData.formJSON.inputs[i].value = $('#PreBotSurvey #formChatBotVisibility').val();
                break;
            case "chatBotHoldup":
                iData.formJSON.inputs[i].options = [{ text: "false", value: "false" }, { text: "true", value: "true" }];
                if ($('#PreBotSurvey #formChatBotHoldup').is(':checked')) {
                    iData.formJSON.inputs[i].options[1].selected = "selected";
                } else {
                    iData.formJSON.inputs[i].options[0].selected = "selected";
                }
                break;
            case "stopBotOnAgentArrival":
                iData.formJSON.inputs[i].options = [{ text: "false", value: "false" }, { text: "true", value: "true" }];
                if ($('#PreBotSurvey #formStopBotOnAgentArrival').is(':checked')) {
                    iData.formJSON.inputs[i].options[1].selected = "selected";
                } else {
                    iData.formJSON.inputs[i].options[0].selected = "selected";
                }
                break;
            case "stopBotOnCustomerLeft":
                iData.formJSON.inputs[i].options = [{ text: "false", value: "false" }, { text: "true", value: "true" }];
                if ($('#PreBotSurvey #formStopBotOnCustomerLeft').is(':checked')) {
                    iData.formJSON.inputs[i].options[1].selected = "selected";
                } else {
                    iData.formJSON.inputs[i].options[0].selected = "selected";
                }
                break;
            case "chatBotMediaOrigin":
                iData.formJSON.inputs[i].value = $('#PreBotSurvey #formMediaOrigin').val();
                break;
            default:
                break;
        }
    }

    // iData.userData kept intact

}


$(document).ready(function () {

    UI_Init();

});

// ##### Widget Define Local Customization

window._genesys.widgets.onReady = function (QuickBus) {

    UI_SetWidgetsConfig();

    localWidgetPlugin = CXBus.registerPlugin('MyLocalCustomization');

    localWidgetPlugin.subscribe("WebChat.ready", function (e) {

    });

    localWidgetPlugin.before("WebChat.open", function (oData) {

        // openchat vs openbotservice
        if (oData) {
            if (oData.customType && oData.customType == "bot") {
                UI_ChatForm_BotService(oData);
            } else {
                // Assume standard Chat request
                UI_ChatForm_Standard(oData);
            }
        }

        return oData;
    });

    if (iPreloadChatWidget) {
        CXBus.loadPlugin("webchat");
    }

};




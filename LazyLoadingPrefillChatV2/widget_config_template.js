/***********************************************************************
 * Copyright Genesys Laboratories. All Rights Reserved
 ************************************************************************/

// ##### Widget Define UI
// Global Genesys Object
if (!window._genesys)
    window._genesys = {};

if (!window._gt)
    window._gt = [];

if (!window._genesys.widgets)
    window._genesys.widgets = {};

// Initialize per widget - not to override extensions if defined before.
//Initializes widgets configuration object.
window._genesys.widgets.main = {
    debug: true,
    theme: 'dark',
    lang: 'en',
    mobileMode: 'auto',
    mobileModeBreakpoint: 600
};

window._genesys.widgets.webchat = {
    dataURL: 'YOUR GMS CHAT SERVICE URL',
    apikey: '', // For Apigee service only
    userData: {},
    emojis: true,
    actionsMenu: true,
    uploadsEnabled: false,
    cometD: {
        enabled: false,
        cometURL: 'YOUR GMS CHAT SERVICE COMET URL',
        channel: 'YOUR GMS CHAT SERVICE COMET CHANNEL',
        apiURL: '',
        websocketEnabled: false,
        logLevel: 'debug'
    },
    autoInvite: {
        enabled: false,
        timeToInviteSeconds: 5,
        inviteTimeoutSeconds: 30
    },
    chatButton: {
        enabled: false,
        // template: '<div>CHAT NOW</div>',
        openDelay: 100,
        effectDuration: 100,
        hideDuringInvite: true
    }
};

window._genesys.widgets.calendar = {
    showAvailability: false,
    numberOfDays: 5,
    timeFormat: 12,
    calendarHours: {
        interval: 10,
        morning: {
            enable: true,
            openTime: '09:00',
            closeTime: '11:59'
        },
        afternoon: {
            enable: true,
            openTime: '12:00',
            closeTime: '16:59'
        },
        evening: {
            enable: true,
            openTime: '17:00',
            closeTime: '23:59'
        }
    }
};

window._genesys.widgets.stats = {
    ajaxTimeout: 3000,
    ewt: {
        dataURL: 'YOUR GMS STATS SERVICE URL',
        apikey: ''
    }
};

window._genesys.widgets.channelselector = {
    ewtRefreshInterval: 10,
    channels: [
        {
            enable: true,
            clickCommand: 'TestExtension.open',
            readyEvent: 'TestExtension.ready',
            displayName: 'Test Extension',
            i10n: "VideoTitle",
            icon: "videochat",
            ewt: {
                display: true,
                queue: 'callus_ewt_test_eservices',
                availabilityThresholdMin: 300,
                availabilityThresholdMax: 480,
                hideChannelWhenThresholdMax: false
            }
        },
        {
            enable: true,
            clickCommand: 'WebChat.open',
            readyEvent: 'WebChat.ready',
            displayName: 'Web Chat',
            i18n: 'ChatTitle',
            icon: 'chat',
            ewt: {
                display: true,
                queue: 'chat_ewt_test_eservices',
                availabilityThresholdMin: 300,
                availabilityThresholdMax: 480,
                hideChannelWhenThresholdMax: false
            }
        },
        {
            enable: true,
            clickCommand: 'WebChat.open',
            clickOptions: { customType: "bot" },
            readyEvent: 'WebChat.ready',
            displayName: 'Bot Service',
            i18n: 'ChatTitle',
            icon: 'chat',
            ewt: {
                display: true,
                queue: 'chat_ewt_test_eservices',
                availabilityThresholdMin: 300,
                availabilityThresholdMax: 480,
                hideChannelWhenThresholdMax: false
            }
        }
    ]
};

window._genesys.widgets.sidebar = {
    showOnStartup: true,
    position: "right",
    expandOnHover: true,
    channels: [
        {
            name: "ChannelSelector"
        },
        {
            name: 'TestExtension',
            clickCommand: 'TestExtension.open',
            displayName: 'Test Extension',
            displayTitle: 'Request TestExtension',
            icon: 'videochat'
        }
    ]
};

window._genesys.widgets.main.preload = [
    "sidebar"
];


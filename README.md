# README #

Examples of customization with Genesys Widgets.

### Widgets Customization Examples IV ###

*This example, built using Genesys Widgets 9.0.003.03, presents the following capabilities:*
#### Lazy-Loading approach to load Genesys Widgets libraries (CXBus and plugings):
CXBus is loaded immediately (cxbus.min.js) - the different widgets/plugins depending on use (pre-load or on first request).

#### Example of Custom Extension/Plugin definition:
The custom plugin (TestExtension) is defined in lib\plugins\testextension.min.js (opening of a Toaster).
	
#### Use of a Custom Chat Registration Form
Access the standard Chat registration Form and to a custom one (Bot Service in this example) is provided via Buttons and ChannelSelector.

#### Pre-fill of Chat Registration Form (standard and custom) using CXBus *before* command:
The sample shows the use of the newly introduced CXBus before command.  
When used on "WebChat.open", it allows to inject code and to modify the data parameters (containing the form/formJSON structure) before the Chat Registration Form is displayed.

#### Ready for Chat v2 (with or without CometD)
The sample supports the use of Chat v2, with or without CometD (polling, cometd with long-polling, cometd with websocket), if configured and enabled properly in GMS and Chat Server.

Note that Genesys Widgets libraries and css are not provided in this repository, and will have to be installed separately.

### How do I get set up? ###

#### Summary of set up

Get the source for the different examples in this repository.

Download Genesys Widgets 9.0.003.03 and copy:

* widgets.min.css into the \stylesheets directory

* cxbus.min.js into the \lib directory, and the different plugins into the lib\plugins directory (Lazy-Loading approach).

Genesys Widgets libraries and css are not provided in this repository.

#### Configuration

Default widgets and user parameters are defined at the top of the widget_main.js file.  
Change the service URLs according to your environment.

The WebChat, SideBar, Channel Selector and TestExtension widgets are configured and enabled in the sample.

Note that even if you don’t have a valid URL for Chat Service – ex: service not configured in GMS, in your environment - you can still play with the widgets (open, close, prefill form, …).

### Available Customization Examples IV ###

* Chat and Bot Service sample


